package ru.t1.bugakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.model.Project;

import java.sql.SQLException;

public interface IProjectRepository extends IAbstractUserOwnedRepository<Project> {

    void update(@NotNull Project project) throws SQLException;
}
