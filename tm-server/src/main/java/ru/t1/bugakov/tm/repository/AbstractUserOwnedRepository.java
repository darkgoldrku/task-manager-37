package ru.t1.bugakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.IAbstractUserOwnedRepository;
import ru.t1.bugakov.tm.constacts.DBConstants;
import ru.t1.bugakov.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IAbstractUserOwnedRepository<M> {

    @NotNull
    private final String USER_ID_COLUMN = DBConstants.COLUMN_USER_ID;

    public AbstractUserOwnedRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    public abstract M add(@NotNull final String userId, @NotNull final M model) throws SQLException;

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator comparator) throws SQLException {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull String sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        if (comparator != null) sql = String.format(sql + "ORDER BY %s", getSortType(comparator));
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @Override
    public int getSize(@NotNull final String userId) throws SQLException {
        @NotNull final String sql = String.format("SELECT COUNT(*) FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @Nullable
    @Override
    public M findById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE id = ? AND %s = ? LIMIT 1", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public M findByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1 OFFSET ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setInt(2, index - 1);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            for (int i = 0; i < index - 1; i++) {
                resultSet.next();
            }
            return fetch(resultSet);
        }
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        return findById(userId, id) != null;
    }

    @Override
    public void clear(@NotNull final String userId) throws SQLException {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) throws SQLException {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE id = ? AND %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, userId);
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @Nullable final M model = findById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @Nullable final M model = findByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public void removeAll(@NotNull final String userId, @NotNull final Collection<M> models) throws SQLException {
        for (M model : models) {
            remove(userId, model);
        }
    }

}
