package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.model.User;

import java.sql.SQLException;

public interface IUserService extends IAbstractService<User> {

    @NotNull
    User create(@Nullable final String login, @Nullable final String password) throws SQLException;

    @NotNull
    User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws SQLException;

    @NotNull
    User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws SQLException;

    @NotNull
    User findByLogin(@Nullable final String login) throws SQLException;

    @NotNull
    User findByEmail(@Nullable final String email) throws SQLException;

    @Nullable
    User removeByLogin(@Nullable final String login) throws SQLException;

    @Nullable
    User removeByEmail(@Nullable final String email) throws SQLException;

    @NotNull
    User setPassword(@Nullable final String id, @Nullable String password) throws SQLException;

    @NotNull
    User updateUser(@Nullable final String id,
                    @Nullable final String firstName,
                    @Nullable final String lastName,
                    @Nullable final String middleName) throws SQLException;

    boolean isLoginExist(@Nullable final String login) throws SQLException;

    boolean isEmailExist(@Nullable final String email) throws SQLException;

    @Nullable
    User lockUserByLogin(@Nullable final String login) throws SQLException;

    @Nullable
    User unlockUserByLogin(@Nullable final String login) throws SQLException;
}
