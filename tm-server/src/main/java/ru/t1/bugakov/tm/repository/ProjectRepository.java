package ru.t1.bugakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.repository.IProjectRepository;
import ru.t1.bugakov.tm.constacts.DBConstants;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.model.Project;

import java.sql.*;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return DBConstants.TABLE_PROJECT;
    }

    @Override
    public Project fetch(@NotNull ResultSet row) throws SQLException {
        @NotNull final Project project = new Project();
        project.setId(row.getString(DBConstants.COLUMN_ID));
        project.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        project.setName(row.getString(DBConstants.COLUMN_NAME));
        project.setDescription(row.getString(DBConstants.COLUMN_DESCRIPTION));
        project.setStatus(Status.toStatus(row.getString(DBConstants.COLUMN_STATUS)));
        project.setCreated(row.getTimestamp(DBConstants.COLUMN_CREATED));
        return project;
    }

    @NotNull
    @Override
    public Project add(@NotNull Project project) throws SQLException {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s) VALUES (?,?,?,?,?,?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_NAME,
                DBConstants.COLUMN_DESCRIPTION, DBConstants.COLUMN_STATUS, DBConstants.COLUMN_CREATED);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setString(2, project.getUserId());
            statement.setString(3, project.getName());
            statement.setString(4, project.getDescription());
            statement.setString(5, project.getStatus().name());
            statement.setTimestamp(6, new Timestamp(project.getCreated().getTime()));
            statement.executeUpdate();
            return project;
        }
    }

    @Override
    public @NotNull Project add(@NotNull String userId, @NotNull Project project) throws SQLException {
        project.setUserId(userId);
        return add(project);
    }

    @Override
    public void update(@NotNull final Project project) throws SQLException {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_NAME,
                DBConstants.COLUMN_DESCRIPTION, DBConstants.COLUMN_STATUS,
                DBConstants.COLUMN_CREATED, DBConstants.COLUMN_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getUserId());
            statement.setString(2, project.getName());
            statement.setString(3, project.getDescription());
            statement.setString(4, project.getStatus().name());
            statement.setTimestamp(5, new Timestamp(project.getCreated().getTime()));
            statement.setString(6, project.getId());
            statement.executeUpdate();
        }
    }

}
