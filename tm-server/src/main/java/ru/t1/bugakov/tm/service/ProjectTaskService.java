package ru.t1.bugakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.ITaskRepository;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.api.service.IProjectService;
import ru.t1.bugakov.tm.api.service.IProjectTaskService;
import ru.t1.bugakov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.bugakov.tm.exception.entity.TaskNotFoundException;
import ru.t1.bugakov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.bugakov.tm.exception.field.TaskIdEmptyException;
import ru.t1.bugakov.tm.exception.field.UserIdEmptyException;
import ru.t1.bugakov.tm.model.Task;
import ru.t1.bugakov.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class ProjectTaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements IProjectTaskService {

    @NotNull
    private final IProjectService projectService;

    public ProjectTaskService(@NotNull IConnectionService connectionService, @NotNull IProjectService projectService) {
        super(connectionService);
        this.projectService = projectService;
    }

    @Override
    public @NotNull ITaskRepository getRepository(@NotNull Connection connection) {
        return new TaskRepository(connection);
    }

    @Override
    public void bindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            @Nullable final Task task = repository.findById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void unbindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            @Nullable final Task task = repository.findById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            @NotNull final List<Task> tasks = repository.findAllByProjectId(userId, projectId);
            for (@NotNull final Task task : tasks) repository.remove(userId, task);
            projectService.removeById(userId, projectId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
