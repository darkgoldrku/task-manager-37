package ru.t1.bugakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.model.Task;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository extends IAbstractUserOwnedRepository<Task> {

    void update(@NotNull Task task) throws SQLException;

    @NotNull
    List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException;

}
