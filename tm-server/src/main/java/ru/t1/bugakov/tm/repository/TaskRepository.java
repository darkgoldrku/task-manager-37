package ru.t1.bugakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.repository.ITaskRepository;
import ru.t1.bugakov.tm.constacts.DBConstants;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.model.Task;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return DBConstants.TABLE_TASK;
    }

    @Override
    public Task fetch(@NotNull ResultSet row) throws SQLException {
        @NotNull final Task task = new Task();
        task.setId(row.getString(DBConstants.COLUMN_ID));
        task.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        task.setProjectId(row.getString(DBConstants.COLUMN_PROJECT_ID));
        task.setName(row.getString(DBConstants.COLUMN_NAME));
        task.setDescription(row.getString(DBConstants.COLUMN_DESCRIPTION));
        task.setStatus(Status.toStatus(row.getString(DBConstants.COLUMN_STATUS)));
        task.setCreated(row.getTimestamp(DBConstants.COLUMN_CREATED));
        return task;
    }

    @NotNull
    @Override
    public Task add(@NotNull Task task) throws SQLException {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s) VALUES (?,?,?,?,?,?,?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_USER_ID,
                DBConstants.COLUMN_PROJECT_ID, DBConstants.COLUMN_NAME,
                DBConstants.COLUMN_DESCRIPTION, DBConstants.COLUMN_STATUS,
                DBConstants.COLUMN_CREATED);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setString(2, task.getUserId());
            statement.setString(3, task.getProjectId());
            statement.setString(4, task.getName());
            statement.setString(5, task.getDescription());
            statement.setString(6, task.getStatus().name());
            statement.setTimestamp(7, new Timestamp(task.getCreated().getTime()));
            statement.executeUpdate();
            return task;
        }
    }

    @Override
    public @NotNull Task add(@NotNull String userId, @NotNull Task task) throws SQLException {
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public void update(@NotNull final Task task) throws SQLException {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_USER_ID,
                DBConstants.COLUMN_PROJECT_ID, DBConstants.COLUMN_NAME,
                DBConstants.COLUMN_DESCRIPTION, DBConstants.COLUMN_STATUS,
                DBConstants.COLUMN_CREATED, DBConstants.COLUMN_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getUserId());
            statement.setString(2, task.getProjectId());
            statement.setString(3, task.getName());
            statement.setString(4, task.getDescription());
            statement.setString(5, task.getStatus().name());
            statement.setTimestamp(6, new Timestamp(task.getCreated().getTime()));
            statement.setString(7, task.getId());
            statement.executeUpdate();
        }
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? AND %s = ?",
                getTableName(), DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_PROJECT_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

}