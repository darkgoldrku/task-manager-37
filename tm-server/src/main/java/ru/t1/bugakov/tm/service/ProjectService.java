package ru.t1.bugakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.IProjectRepository;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.api.service.IProjectService;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.bugakov.tm.exception.field.*;
import ru.t1.bugakov.tm.model.Project;
import ru.t1.bugakov.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.sql.SQLException;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    public IProjectRepository getRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            project = repository.add(userId, project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        @Nullable Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

}
