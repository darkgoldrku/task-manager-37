package ru.t1.bugakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.model.AbstractModel;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IAbstractRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull final M model) throws SQLException;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models) throws SQLException;

    @NotNull
    Collection<M> set(@NotNull Collection<M> models) throws SQLException;

    @NotNull
    List<M> findAll() throws SQLException;

    @NotNull
    List<M> findAll(@NotNull final Comparator<M> comparator) throws SQLException;

    int getSize() throws SQLException;

    @Nullable
    M findById(@NotNull final String id) throws SQLException;

    @Nullable
    M findByIndex(@NotNull final Integer index) throws SQLException;

    boolean existsById(@NotNull final String id) throws SQLException;

    void clear() throws SQLException;

    @Nullable
    M remove(@NotNull final M model) throws SQLException;

    @Nullable
    M removeById(@NotNull final String id) throws SQLException;

    @Nullable
    M removeByIndex(@NotNull final Integer index) throws SQLException;

    void removeAll(@NotNull Collection<M> models) throws SQLException;
}
