package ru.t1.bugakov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.api.service.IDataBaseProperty;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IDataBaseProperty dataBaseProperties;

    public ConnectionService(@NotNull IDataBaseProperty dataBaseProperties) {
        this.dataBaseProperties = dataBaseProperties;
    }

    @Override
    @NotNull
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final String username = dataBaseProperties.getDatabaseUsername();
        @NotNull final String password = dataBaseProperties.getDatabasePassword();
        @NotNull final String url = dataBaseProperties.getDatabaseUrl();
        @NotNull final Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
