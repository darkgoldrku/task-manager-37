package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.model.Project;

import java.sql.SQLException;

public interface IProjectService extends IAbstractUserOwnedService<Project> {

    @NotNull
    Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws SQLException;

    @NotNull
    Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) throws SQLException;

    @NotNull
    Project updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) throws SQLException;

    @NotNull
    Project changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) throws SQLException;

    @NotNull
    Project changeProjectStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) throws SQLException;

}
