package ru.t1.bugakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.IAbstractRepository;
import ru.t1.bugakov.tm.api.repository.IAbstractUserOwnedRepository;
import ru.t1.bugakov.tm.api.service.IAbstractUserOwnedService;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.exception.entity.ModelNotFoundException;
import ru.t1.bugakov.tm.exception.field.IdEmptyException;
import ru.t1.bugakov.tm.exception.field.IndexIncorrectException;
import ru.t1.bugakov.tm.exception.field.UserIdEmptyException;
import ru.t1.bugakov.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IAbstractUserOwnedRepository<M>> extends AbstractService<M, R> implements IAbstractUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    public abstract IAbstractUserOwnedRepository<M> getRepository(@NotNull final Connection connection);

    @NotNull
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            IAbstractUserOwnedRepository<M> repository = getRepository(connection);
            repository.add(userId, model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findAll(userId);
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findAll(userId, comparator);
        }
    }

    @Override
    public int getSize(@Nullable final String userId) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository(connection);
            return repository.getSize(userId);
        }
    }

    @Nullable
    @Override
    public M findById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findById(userId, id);
        }
    }

    @Nullable
    @Override
    public M findByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findByIndex(userId, index);
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository(connection);
            return repository.existsById(userId, id);
        }
    }

    @Override
    public void clear(@Nullable final String userId) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository(connection);
            repository.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository(connection);
            repository.remove(userId, model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        @Nullable M model;
        try {
            IAbstractUserOwnedRepository<M> repository = getRepository(connection);
            model = repository.removeById(userId, id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException {
        if (userId == null) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final Connection connection = getConnection();
        @Nullable M model;
        try {
            IAbstractUserOwnedRepository<M> repository = getRepository(connection);
            model = repository.removeByIndex(userId, index);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Override
    public void removeAll(@Nullable final String userId, @Nullable final Collection<M> models) throws SQLException {
        if (models == null) throw new ModelNotFoundException();
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            repository.removeAll(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
