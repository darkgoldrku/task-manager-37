package ru.t1.bugakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.model.Session;

import java.sql.SQLException;

public interface ISessionRepository extends IAbstractUserOwnedRepository<Session> {
    void update(@NotNull Session session) throws SQLException;
}
