package ru.t1.bugakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.model.AbstractUserOwnedModel;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IAbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends IAbstractRepository<M> {

    @NotNull
    M add(@NotNull final String userId, @NotNull final M model) throws SQLException;

    @NotNull
    List<M> findAll(@NotNull final String userId) throws SQLException;

    @NotNull
    List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) throws SQLException;

    int getSize(@NotNull final String userId) throws SQLException;

    @Nullable
    M findById(@NotNull final String userId, @NotNull final String id) throws SQLException;

    @Nullable
    M findByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException;

    boolean existsById(@NotNull final String userId, @NotNull final String id) throws SQLException;

    void clear(@NotNull final String userId) throws SQLException;

    @Nullable
    M remove(@Nullable final String userId, @Nullable final M model) throws SQLException;

    @Nullable
    M removeById(@NotNull final String userId, @NotNull final String id) throws SQLException;

    @Nullable
    M removeByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException;

    void removeAll(@NotNull String userId, @NotNull Collection<M> models) throws SQLException;
}
