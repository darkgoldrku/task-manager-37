package ru.t1.bugakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.model.User;

import java.sql.SQLException;

public interface IUserRepository extends IAbstractRepository<User> {

    void update(@NotNull User user) throws SQLException;

    @Nullable
    User findByLogin(@NotNull final String login) throws SQLException;

    @Nullable
    User findByEmail(@NotNull final String email) throws SQLException;

    @NotNull
    Boolean isLoginExist(@NotNull final String login) throws SQLException;

    @NotNull
    Boolean isEmailExist(@NotNull final String email) throws SQLException;

    @Nullable
    User lockUserByLogin(@NotNull final String login) throws SQLException;

    @Nullable
    User unlockUserByLogin(@NotNull final String login) throws SQLException;

}
