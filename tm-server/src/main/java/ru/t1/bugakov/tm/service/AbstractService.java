package ru.t1.bugakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.IAbstractRepository;
import ru.t1.bugakov.tm.api.service.IAbstractService;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.exception.entity.ModelNotFoundException;
import ru.t1.bugakov.tm.exception.field.IdEmptyException;
import ru.t1.bugakov.tm.exception.field.IndexIncorrectException;
import ru.t1.bugakov.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IAbstractRepository<M>> implements IAbstractService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    protected AbstractService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    protected abstract IAbstractRepository<M> getRepository(@NotNull final Connection connection);

    @NotNull
    @Override
    public M add(@Nullable final M model) throws SQLException {
        if (model == null) throw new ModelNotFoundException();
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            repository.add(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) throws SQLException {
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            repository.add(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) throws SQLException {
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            repository.set(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll() throws SQLException {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws SQLException {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            if (comparator == null) return findAll();
            return repository.findAll(comparator);
        }
    }

    @Override
    public int getSize() throws SQLException {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            return repository.getSize();
        }
    }

    @Nullable
    @Override
    public M findById(@Nullable final String id) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            return repository.findById(id);
        }
    }

    @Nullable
    @Override
    public M findByIndex(@Nullable final Integer index) throws SQLException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            return repository.findByIndex(index);
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            return repository.existsById(id);
        }
    }

    @Override
    public void clear() throws SQLException {
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) throws SQLException {
        if (model == null) throw new ModelNotFoundException();
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            repository.remove(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M model;
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            model = repository.removeById(id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) throws SQLException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable M model;
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            model = repository.removeByIndex(index);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Override
    public void removeAll(@Nullable final Collection<M> models) throws SQLException {
        if (models == null) throw new ModelNotFoundException();
        @NotNull Connection connection = getConnection();
        try {
            @NotNull final IAbstractRepository<M> repository = getRepository(connection);
            repository.removeAll(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
