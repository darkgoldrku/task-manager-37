package ru.t1.bugakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.IUserRepository;
import ru.t1.bugakov.tm.constacts.DBConstants;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return DBConstants.TABLE_USER;
    }

    @Override
    public User fetch(@NotNull ResultSet row) throws SQLException {
        @NotNull final User user = new User();
        user.setId(row.getString(DBConstants.COLUMN_ID));
        user.setLogin(row.getString(DBConstants.COLUMN_LOGIN));
        user.setPasswordHash(row.getString(DBConstants.COLUMN_PASSWORD));
        user.setEmail(row.getString(DBConstants.COLUMN_EMAIL));
        user.setRole(Role.valueOf(row.getString(DBConstants.COLUMN_ROLE)));
        user.setLocked(row.getBoolean(DBConstants.COLUMN_LOCKED));
        user.setFirstName(row.getString(DBConstants.COLUMN_FIRST_NAME));
        user.setLastName(row.getString(DBConstants.COLUMN_LAST_NAME));
        user.setMiddleName(row.getString(DBConstants.COLUMN_MIDDLE_NAME));
        return user;
    }

    @NotNull
    @Override
    public User add(@NotNull User user) throws SQLException {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (?,?,?,?,?,?,?,?,?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_LOGIN, DBConstants.COLUMN_PASSWORD,
                DBConstants.COLUMN_EMAIL, DBConstants.COLUMN_ROLE, DBConstants.COLUMN_LOCKED,
                DBConstants.COLUMN_FIRST_NAME, DBConstants.COLUMN_LAST_NAME, DBConstants.COLUMN_MIDDLE_NAME);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getEmail());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getRole().getDisplayName());
            statement.setBoolean(6, user.isLocked());
            statement.setString(7, user.getFirstName());
            statement.setString(8, user.getLastName());
            statement.setString(9, user.getMiddleName());
            statement.executeUpdate();
            return user;
        }
    }

    @Override
    public void update(@NotNull final User user) throws SQLException {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_LOGIN, DBConstants.COLUMN_PASSWORD, DBConstants.COLUMN_EMAIL,
                DBConstants.COLUMN_ROLE, DBConstants.COLUMN_LOCKED, DBConstants.COLUMN_FIRST_NAME,
                DBConstants.COLUMN_LAST_NAME, DBConstants.COLUMN_MIDDLE_NAME, DBConstants.COLUMN_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPasswordHash());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getRole().getDisplayName());
            statement.setBoolean(5, user.isLocked());
            statement.setString(6, user.getFirstName());
            statement.setString(7, user.getLastName());
            statement.setString(8, user.getMiddleName());
            statement.setString(9, user.getId());
            statement.executeUpdate();
        }
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) throws SQLException {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1", getTableName(), DBConstants.COLUMN_LOGIN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) throws SQLException {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1", getTableName(), DBConstants.COLUMN_EMAIL);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) throws SQLException {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) throws SQLException {
        return findByEmail(email) != null;
    }

    @Nullable
    @Override
    public User lockUserByLogin(@NotNull final String login) throws SQLException {
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(true);
        update(user);
        return user;
    }

    @Nullable
    @Override
    public User unlockUserByLogin(@NotNull final String login) throws SQLException {
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(false);
        update(user);
        return user;
    }

}