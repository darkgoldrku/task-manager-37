package ru.t1.bugakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.repository.ISessionRepository;
import ru.t1.bugakov.tm.constacts.DBConstants;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.model.Session;

import java.sql.*;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return DBConstants.TABLE_SESSION;
    }

    @Override
    public Session fetch(@NotNull ResultSet row) throws SQLException {
        @NotNull final Session session = new Session();
        session.setId(row.getString(DBConstants.COLUMN_ID));
        session.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        session.setDate(row.getTimestamp(DBConstants.COLUMN_CREATED));
        session.setRole(Role.valueOf(DBConstants.COLUMN_ROLE));
        return session;
    }

    @Override
    public Session add(@NotNull Session session) throws SQLException {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s) VALUES (?,?,?,?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_USER_ID,
                DBConstants.COLUMN_CREATED, DBConstants.COLUMN_ROLE);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getId());
            statement.setString(2, session.getUserId());
            statement.setTimestamp(3, new Timestamp(session.getDate().getTime()));
            statement.setString(4, session.getRole().getDisplayName());
            statement.executeUpdate();
            return session;
        }
    }

    @Override
    public @NotNull Session add(@NotNull String userId, @NotNull Session session) throws SQLException {
        session.setUserId(userId);
        return add(session);
    }

    @Override
    public void update(@NotNull final Session session) throws SQLException {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_CREATED, DBConstants.COLUMN_USER_ID,
                DBConstants.COLUMN_ROLE, DBConstants.COLUMN_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setTimestamp(1, new Timestamp(session.getDate().getTime()));
            statement.setString(2, session.getUserId());
            statement.setString(3, session.getRole().getDisplayName());
            statement.setString(4, session.getId());
            statement.executeUpdate();
        }
    }

}