package ru.t1.bugakov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCreateRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private String name;

    @Nullable
    private String description;

    public TaskCreateRequest(@Nullable String name, @Nullable String description) {
        this.name = name;
        this.description = description;
    }

    public TaskCreateRequest(@Nullable String token, @Nullable String name, @Nullable String description) {
        super(token);
        this.name = name;
        this.description = description;
    }

}
