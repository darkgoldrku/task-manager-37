package ru.t1.bugakov.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;
import ru.t1.bugakov.tm.enumerated.ProjectSort;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private ProjectSort projectSort;

    public ProjectListRequest(@Nullable ProjectSort projectSort) {
        this.projectSort = projectSort;
    }

    public ProjectListRequest(@Nullable String token, @Nullable ProjectSort projectSort) {
        super(token);
        this.projectSort = projectSort;
    }

}
