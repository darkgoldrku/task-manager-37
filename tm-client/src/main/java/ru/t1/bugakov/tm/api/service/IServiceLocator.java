package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITokenService getTokenService();

    ISystemEndpoint getSystemEndpoint();

    IAuthEndpoint getAuthEndpoint();

    IDomainEndpoint getDomainEndpoint();

    IProjectEndpoint getProjectEndpoint();

    ITaskEndpoint getTaskEndpoint();

    IUserEndpoint getUserEndpoint();
}
