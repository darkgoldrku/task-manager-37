package ru.t1.bugakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.data.DataXmlLoadFasterXmlRequest;

public final class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        getDomainEndpoint().XmlLoadFasterXml(new DataXmlLoadFasterXmlRequest(getToken()));
    }

    @Override
    public @NotNull
    String getName() {
        return "data-load-xml-fasterxml";
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Load data from xml file with fasterxml";
    }

}
